package id.silmiakamal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @author HP
 */
public class Asrama implements Serializable {

    private final String aplikasi = "Aplikasi Pembayaran Uang Asrama";
    private final String greeting = "Selamat Datang";

    private String nomorkamar, nama, nomorhp;
    private String bulanke;
    private int paketan;
    private LocalDateTime bulanMasuk;
    private LocalDateTime bulanKeluar;
    private BigDecimal biaya;
    private boolean keluar = true;

    public Asrama() {

    }

       public Asrama(String nomorkamar, String nama, String nomorhp, String bulanke, int paketan, LocalDateTime bulanMasuk, LocalDateTime bulanKeluar, BigDecimal biaya) {
        this.nomorkamar = nomorkamar;
        this.nama = nama;
        this.nomorhp = nomorhp;
        this.bulanke = bulanke;
        this.bulanMasuk = bulanMasuk;
        this.bulanKeluar = bulanKeluar;
        this.paketan = paketan;
        this.biaya = biaya;

    }
    public String getAplikasi() {
        return aplikasi;
    }

    public String getGreeting() {
        return greeting;
    }

    public int getPaketan() {
        return paketan;
    }

    public void setPaketan(int paketan) {
        this.paketan = paketan;
    }

    public LocalDateTime getBulanKeluar() {
        return bulanKeluar;
    }

    public void setBulanKeluar(LocalDateTime bulanKeluar) {
        this.bulanKeluar = bulanKeluar;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNomorkamar() {
        return nomorkamar;
    }

    public void setNomorkamar(String nomorkamar) {
        this.nomorkamar = nomorkamar;
    }

    public String getNomorhp() {
        return nomorhp;
    }

    public void setNomorhp(String nomorhp) {
        this.nomorhp = nomorhp;
    }

    public int getpaketan() {
        return paketan;
    }

    public void setpaketan(int paketan) {
        this.paketan = paketan;
    }

    public LocalDateTime getBulanMasuk() {
        return bulanMasuk;
    }

    public void setBulanMasuk(LocalDateTime bulanMasuk) {
        this.bulanMasuk = bulanMasuk;
    }

    public BigDecimal getBiaya() {
        return biaya;
    }

    public void setBiaya(BigDecimal biaya) {
        this.biaya = biaya;
    }

    public boolean isKeluar() {
        return keluar;
    }

    public void setKeluar(boolean Keluar) {
        this.keluar = keluar;
    }

    public String getBulanke() {
        return bulanke;
    }

    public void setBulanke(String bulanke) {
        this.bulanke = bulanke;
    }

    @Override
    public String toString() {
        return "Asrama{" + "nomorkamar=" + nomorkamar + ", nama=" + nama + ", nomorhp =" + nomorhp + ", bulanke=" + bulanke + ", paketan =" + paketan + ",bulanMasuk=" + bulanMasuk + ", biaya=" + biaya + ", keluar=" + keluar + '}';
    }

}
