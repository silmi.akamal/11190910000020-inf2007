package id.silmiakamal.Controller;

import id.silmiakamal.model.Asrama;
import java.util.Scanner;


/**
 *
 * @author HP
 */
public class Menu {

    private final Scanner in = new Scanner(System.in);
    private int noMenu;

    public void getMenuAwal() {
        Asrama info = new Asrama();

        System.out.println("=================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getGreeting());
        System.out.println("-------------------------------------------------");

        System.out.println("DAFTAR MENU");
        System.out.println("1. Menu Masuk Asrama");
        System.out.println("2. Laporan Bulanan");
        System.out.println("3. Keluar Aplikasi");
        System.out.println("=================================================");
        System.out.print("Pilih Menu (1/2/3) : ");

        do {
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Pilih Menu (1/2/3) : ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu();
    }

    public void setPilihMenu() {
        AsramaController pc = new AsramaController();
        switch (noMenu) {
            case 1:
                pc.setMasukAsrama();
                break;
           case 2:
                pc.getDataBulanan();
                break;
            case 3:
                System.out.println("Sampai jumpa :)");
                System.exit(0);
                break;
        }
    }
}
