package id.silmiakamal.Controller;

import com.google.gson.Gson;
import id.silmiakamal.model.Asrama;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author HP
 */
public class AsramaController {

    private static final String FILE = "D:\\asrama.json";
    private Asrama asrama;
    private final Scanner in;
    private String nama;
    private String nomorkamar, nomorhp, bulanke;
    private int paketan;
    private LocalDateTime bulanMasuk;
    private BigDecimal biaya;
    private final DateTimeFormatter dateTimeFormat;
    private int pilihan;
    private boolean keluar = false;

    public AsramaController() {
        in = new Scanner(System.in);
        bulanMasuk = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("MM-yyyy");
    }

    /*
     * Method untuk menginput data masuk asrama
     **/
    public void setMasukAsrama() {
        System.out.print("Masukkan Nomor Kamar : ");
        nomorkamar = in.next();

        System.out.print("Masukkan Nama: ");
        nama = in.next();

        System.out.print("Masukkan Nomor HP : ");
        nomorhp = in.next();

        String formatbulanMasuk = bulanMasuk.format(dateTimeFormat);
        System.out.println("Waktu Masuk : " + formatbulanMasuk);

        System.out.println("Pembayaran bulan :");
        bulanke = in.next();

        System.out.println("Jenis Pembayaran ");
        System.out.print("1) Asrama, 2) Asrama + makan, 3) Asrama + laundry 4) Asrama + laundry + makan : ");
        paketan = in.nextInt();
        if (paketan == 1) {
            biaya = new BigDecimal(700000);
        } else if (paketan == 2) {
            biaya = new BigDecimal(1000000);
        } else if (paketan == 3) {
            biaya = new BigDecimal(900000);
        } else if (paketan == 4) {
            biaya = new BigDecimal(1500000);
        }
        System.out.println("Biaya : " + biaya);

        asrama = new Asrama();
        asrama.setNomorkamar(nomorkamar);
        asrama.setNama(nama);
        asrama.setNomorhp(nomorhp);
        asrama.setBulanke(bulanke);
        asrama.setpaketan(paketan);
        asrama.setBulanMasuk(bulanMasuk);
        asrama.setBiaya(biaya);
        asrama.setKeluar(true);
        System.out.println("Proses Bayar?");
        System.out.print("1) Ya, 2) Tidak, 3) Keluar : ");
        pilihan = in.nextInt();
        switch (pilihan) {
            case 1:
                System.out.println("Lunas Bulan" + bulanke);
                setWriteAsrama(FILE, asrama);
                break;
            case 2:
                setMasukAsrama();
                break;
            default:
                Menu m = new Menu();
                m.getMenuAwal();
                break;
        }
        System.out.println("Apakah mau Input kembali?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setMasukAsrama();
        }
    }

    public void setWriteAsrama(String file, Asrama asrama) {
        Gson gson = new Gson();

        List<Asrama> asramas = getReadAsrama(file);
        asramas.remove(asrama);
        asramas.add(asrama);

        String json = gson.toJson(asramas);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();

        } catch (IOException ex) {
            Logger.getLogger(AsramaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Asrama> getReadAsrama(String file) {
        List<Asrama> asramas = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try (Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                Asrama[] s = gson.fromJson(line, Asrama[].class);
                asramas.addAll(Arrays.asList(s));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AsramaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AsramaController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return asramas;
    }

    public void getDataBulanan() {
        List<Asrama> asramas = getReadAsrama(FILE);
        Predicate<Asrama> isKeluar = e -> e.isKeluar() == true;
        Predicate<Asrama> isDate = e -> e.getBulanMasuk().toLocalDate().equals(LocalDate.now());

        List<Asrama> aResults = asramas.stream().filter(isKeluar.and(isDate)).collect(Collectors.toList());
        BigDecimal total = aResults.stream()
                .map(Asrama::getBiaya)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("Nomor Kamar \tNama \t\tNomor Hp \t\tWaktu Masuk Asrama \tPembayaran Bulan  \tBiaya ");
        System.out.println("----------- \t---- \t\t-------- \t\t------------------ \t----------------  \t----   ");
        aResults.forEach((a) -> {
        System.out.println(a.getNomorkamar() + "\t\t" + a.getNama() + "\t\t" + a.getNomorhp() + "\t\t\t" + a.getBulanMasuk().format(dateTimeFormat) + " \t\t\t" + a.getBulanke() + "\t\t" + a.getBiaya());
        });
        System.out.println("----------- \t---- \t\t-------- \t\t------------------ \t----------------  \t----   ");
        System.out.println("====================================");
        System.out.println("Pendapatan Total = Rp. " + total);
        System.out.println("====================================");

        System.out.println("Apakah mau mengulanginya?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            getDataBulanan();
        }
    }
}
