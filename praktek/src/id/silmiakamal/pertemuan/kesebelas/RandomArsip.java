package id.silmiakamal.pertemuan.kesebelas;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 *
 * @author HP
 */
public class RandomArsip {

    public void setTulis(String file, int position, String record) {
        try {
            RandomAccessFile raf = new RandomAccessFile(file, "rw");
            //move file pointer to position specified
            raf.seek(position);
            //writing string to RandomAccesFile
            raf.writeUTF(record);
            raf.close();
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }

    }

    public String getBaca(String file, int position) {
        String record = "";
        try {
            RandomAccessFile raf = new RandomAccessFile(file, "rw");
            // move file pointer to position
            raf.seek(position);
//reading
            record = raf.readUTF();
            raf.close();
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
        return record;
    }

    public static void main(String[] args) {
        String berkas = "D:\\Folder\\random.txt";
        String data = "NIM : 123 | Nama : Silmi ";

        RandomArsip ra = new RandomArsip();
        ra.setTulis(berkas, 1, data);
        System.out.println("Tulis berhasil");
        
        String output = ra.getBaca(berkas, 1);
        System.out.println("Baca berhasil : " + output);

    }
}
