package id.silmiakamal.pertemuan.kedelapan;

/**
 *
 * @author HP
 */
public class PangkatDua {

    public static void main(String[] args) {
        int i, k;
        int pangkat[] = new int[10];

        for (i = 0; i < 10; i++) {
            k = i + 1;
            pangkat[1] = k * k;
            System.out.println("Hasil pangkat " + k + " adalah " + pangkat[1]);
        }
    }
}
