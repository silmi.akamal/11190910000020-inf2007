package id.silmiakamal.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author HP
 */
public class SequentialSearchLast {
      public int getSeqSearch(int L[], int n, int x) {
        int i = n - 1;
        boolean ketemu = false;
        while ((i > -1) && (i <= n - 1) && (!ketemu)) {
            if (i <= n - 1) {
                System.out.println("posisi ke " + i + " isi " + L[i]);
            }
            if (L[i] == x) {
                ketemu = true;
            } else {
                i = i - 1;
            }
        }
        if (ketemu) {
            return i;

        } else {
            return -1;
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int L[] =  {13, 16, 14, 21, 76, 15};
        int n, x;
        n = L.length;
        System.out.println("Masukan angka yang ingin dicari = ");
        x = in.nextInt();
        SequentialSearchLast seq = new SequentialSearchLast();
        System.out.println(seq.getSeqSearch(L, n, x));
    }
}


