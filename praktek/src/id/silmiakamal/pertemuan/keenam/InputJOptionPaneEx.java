package id.silmiakamal.pertemuan.keenam;

import javax.swing.JOptionPane;

/**
 *
 * @author HP
 */
public class InputJOptionPaneEx {
    public static void main(String[] args) {
        int bilangan;
        String box= JOptionPane.showInputDialog("Masukkan Bilangan: ");
        
        bilangan = Integer.parseInt(box);
        
        System.out.print("Bilangan: " + bilangan);
            
    }
}
