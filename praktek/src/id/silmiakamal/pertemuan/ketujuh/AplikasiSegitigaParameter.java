package id.silmiakamal.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author HP
 */
public class AplikasiSegitigaParameter {
    public static void main(String[] args) {
        int i, N;
        float a, t;
        Scanner in = new Scanner(System.in);
        System.out.print("Masukan banyaknya segitiga = ");
        N = in.nextInt();

        for (i = 1; i <= N; i++) {
            System.out.println("==Luas Segitiga ==");
            System.out.println("alas= ");
            a = in.nextFloat();
            System.out.println("tinggi = ");
            t = in.nextFloat();
            SegitigaParameter hitungluasSegitiga = new SegitigaParameter (a,t);
        }
    }
}
