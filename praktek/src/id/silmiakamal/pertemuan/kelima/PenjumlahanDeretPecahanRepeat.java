package id.silmiakamal.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author HP
 */
public class PenjumlahanDeretPecahanRepeat {

    public static void main(String[] args) {
        int x;
        float s;
        s = 0;
        Scanner in = new Scanner(System.in);
        System.out.println("x =");
        x = in.nextInt();
        do {

            s = s + (float) 1 / x;
            System.out.println("x=");
            x = in.nextInt();
        } while (x != -1);
        System.out.println("jumlah = " + s);

    }
}
