package id.silmiakamal.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author HP
 */
public class ShellSort {

    int[] getShellSort(int[] L, int n) {
        int i, j, y, step, start;
        boolean ketemu;
        step = n - 1;
        while (step > 1) {
            step = (step / 3) + 1;
            for (start = 0; start < step; start++) {
                i = start + step;
                while (i <= n - 1) {
                    y = L[i];
                    j = i - step;
                    ketemu = false;
                    System.out.println("i: " + i + " j : " + j + " ---> " + L[j]);
                    while ((j >= 0) && (!ketemu)) {
                        if (y < L[j]) {
                            L[j + step] = L[j];
                            j = j - step;
                        } else {
                            ketemu = true;
                            L[j + step] = y;
                            i = i + step;
                        }
                    }
                }
            }
       
        }   
        return L;
    }
        
    public static void main(String[] args) {
        int i, n, j, y;
        int L[] = {25, 27, 10, 8, 76, 21};
        n = L.length;
        System.out.println("SHELL SORT");

        ShellSort app = new ShellSort();
        app.getShellSort(L, n);
        System.out.println(Arrays.toString(L));
    }
}

    

