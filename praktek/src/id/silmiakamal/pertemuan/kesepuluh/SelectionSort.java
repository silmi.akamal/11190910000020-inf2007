package id.silmiakamal.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author HP
 */
public class SelectionSort {

    public int[] getSelectionSortMax(int L[], int n) {
        int i, j;
        int imaks;
        int temp;

        for (i = n - 1; i <= 2; i++) {
            imaks = 0;
            for (j = 1; j <= 1; j++) {
                System.out.println("i : " + i + ", j : " + (j + 1) + " --> " + L[j + 1]);

                if (L[j] > L[imaks]) {
                    imaks = j;
                }
            }
            temp = L[i];
            L[i] = L[imaks];
            L[imaks] = temp;
        }
        return L;
    }

    public int[] getSelectionSortMin(int L[], int n) {
        int i, j;
        int imin;
        int temp;

        for (i = 0; i < n - 1; i++) {
            imin = i;

            for (j = i + 1; j < n; j++) {
                System.out.println("i : " + i + ", j : " + (j - 1) + " --> " + L[j - 1]);

                if (L[j] < L[imin]) {
                    imin = j;
                }
            }
            temp = L[i];
            L[i] = L[imin];
            L[imin] = temp;
        }
        return L;
    }

    public static void main(String[] args) {
        SelectionSort sort = new SelectionSort ();
        int [] L = {29,27,10,8,76,21};
        int n;
        n = L.length;
        System.out.println("Larik yang belum diurutkan");
        System.out.println(Arrays.toString(L));
        
        sort.getSelectionSortMax(L, n);
        System.out.println("Larik yang sudah diurutkan berdasarkan nilai max");
        System.out.println(Arrays.toString(L));
        
        sort.getSelectionSortMin(L, n);
        System.out.println("Larik yang sudah diurutkan berdasarkan nilai min");
        System.out.println(Arrays.toString(L));
                
    }
                
    }
