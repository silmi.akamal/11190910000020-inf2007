package id.silmiakamal.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author HP
 */
public class Bubblesort {

    private long[] getBubblesort;
    public int[] getBubblesort(int L[], int n) {
        int i, k;
        int temp;

        for (i = 0; i < n - 1; i++) {
            for (k = n - 1; k > i; k--) {
                System.out.println("i : " + i + ", k : " + (k - 1) + " --> " + L[k - 1]);

                if (L[k] < L[k - 1]) {
                    temp = L[k];
                    L[k] = L[k - 1];
                    L[k - 1] = temp;
                }
            }
        }
        return L;
    }

    public static void main(String[] args) {
        int i, n;
        int[] L = {25, 27, 10, 8, 76, 21};
        n = L.length;
        System.out.println(" Masukkan Angka : ");

        Bubblesort app = new Bubblesort();
        app.getBubblesort(L, n);

        System.out.println(" Angka : " + Arrays.toString(L));
    }

}
